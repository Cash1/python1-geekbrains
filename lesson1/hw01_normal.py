# coding = UTF-8

__author__ = 'Ruslan Cashaev'

# Задача-1: Дано произвольное целое число, вывести самую большую цифру этого числа.
# Например, дается x = 58375.
# Нужно вывести максимальную цифру в данном числе, т.е. 8.
# Подразумевается, что мы не знаем это число заранее.
# Число приходит в виде целого беззнакового.
# Подсказки:
# * постарайтесь решить задачу с применением арифметики и цикла while;
# * при желании и понимании решите задачу с применением цикла for.

number = 5834975
# решение 1. применил конвертацию в set для сокращения времени работы,
# поскольку длина числа произвольная, а set выдает уникальные значения
print(max([int(_) for _ in set(str(number))]))

# решение 2
res = 0
for _ in str(number):
    if int(_) > res:
        res = int(_)
print(res)

# решение 3
i = 0
res = 0
while i < len(str(number)):
    _ = str(number)[i]
    if int(_) > res:
        res = int(_)
    i += 1
print(res)

# Задача-2: Исходные значения двух переменных запросить у пользователя.
# Поменять значения переменных местами. Вывести новые значения на экран.
# Решите задачу, используя только две переменные.
# Подсказки:
# * постарайтесь сделать решение через действия над числами;
# * при желании и понимании воспользуйтесь синтаксисом кортежей Python.

# решение через операции с числами и две переменные
variableA = input('Значение А: ')
while not variableA.isdigit():
    print('ошибка, нужны только цифры')
    variableA = input('Значение А: ')
variableB = input('Значение B: ')
while not variableB.isdigit():
    print('ошибка, нужны только цифры')
    variableB = input('Значение B: ')
variableB = int(variableB) + int(variableA)
variableA = variableB - int(variableA)
variableB -= variableA
print('Переменная А:', variableA, 'Переменная B:', variableB)

# решение через синтаксис кортежей
# 'Note that multiple assignment is really just a combination of
# tuple packing and sequence unpacking.'
variableA = input('Значение А: ')
variableB = input('Значение B: ')
variableA, variableB = variableB, variableA
print('Меняем переменные местами:')
print('Переменная А:', variableA, 'Переменная B:', variableB)


# Задача-3: Напишите программу, вычисляющую корни квадратного уравнения вида
# ax² + bx + c = 0.
# Коэффициенты уравнения вводятся пользователем.
# Для вычисления квадратного корня воспользуйтесь функцией sqrt() модуля math:
# import math
# math.sqrt(4) - вычисляет корень числа 4

import math

a = int(input('a='))
b = int(input('b='))
c = int(input('c='))
d = b ** 2 - 4 * a * c
x = {}
if d > 0:
    x[1] = (-b + math.sqrt(d)) / (2 * a)
    x[2] = (-b - math.sqrt(d)) / (2 * a)
elif d == 0:
    x[1] = - b / (2 * a)
else:
    pass
print(f'solutions: {x}')
